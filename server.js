const express = require("express");
const app = express();
const ActiveDirectory = require("activedirectory");

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get("/", async (req, res) => {
  var config = {
    url: "ldap://dc.domain.com", // Set your config here
    baseDN: "dc=domain,dc=com",
    username: "username@domain.com",
    password: "password"
  };
  var ad = new ActiveDirectory(config);
  var groupName = "Employees"; // Group name
  ad.getUsersForGroup(groupName, function(err, users) {
    if (err) {
      console.log("ERROR: " + JSON.stringify(err));
      return;
    }

    if (!users) res.send("Group: " + groupName + " not found.");
    else {
      res.send(JSON.stringify(users));
    }
  });
});

app.listen(4000, "0.0.0.0", () =>
  console.log("Node Server running at port 4000")
);
